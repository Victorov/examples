
// Функция рассчитывает необходимую ширину блоков 
// для того что бы они помещались в родительском 
// блоке с одинаковыми отстпами 
//
// пример: pics.sputnik.ru

;(function($){
    function alignmentCalc(options) {
        options = $.extend({
            arr: [],
            wrapper_width: 0,
            outer: 0
        }, options);
        var line = [],
            result = [],
            max_width = 250,
            counter = 0;

        $.each(options.arr, function(i, el){
            var width = Math.max(max_width, el);
            counter += width;
            line.push(width);

            if (counter >= options.wrapper_width) {
                var summ = 0;
                $.each(line, function(j, item) {
                    var wrapper_inner_width = options.wrapper_width - line.length * options.outer;
                    if (j == line.length - 1) {
                        item = wrapper_inner_width - summ;
                    }else{
                        item = Math.floor(item / counter * wrapper_inner_width);
                        summ = summ + item;
                    }
                    result.push(item);
                });
                counter = 0;
                line = [];
            }
        });
        return result;
    }
})(jQuery);



// Модифицированная версия кода счетчика 
// яндекс метрики с очередью на отправление 
// данных о достижении целей

;(function (d, w, c) {
    var rtkCounterId = 38740850,
        push = Array.prototype.push,
        goals_queue = [];

    w.RTKGoal = function(goals2push) {
        goals2push &&
            push.apply(goals_queue, goals2push);
            
        if(w['yaCounter' + rtkCounterId]) {
            while(goals_queue.length) {
                w['yaCounter' + rtkCounterId].reachGoal(goals_queue.shift());
            }
        }
    };

    (w[c] = w[c] || []).push(function() {
        try {
            w['yaCounter' + rtkCounterId] = new Ya.Metrika({
                id: rtkCounterId,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true
            });
            RTKGoal();
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");




// jQuery плагин в декораторе спутника
// для управления фильтрами элементов на странице

;(function($){
    $.fn.filters = sputnik.plugin('app.filters', {
        options: {
            ajax : {
                url: '/filters_json',
                data: {},
                tpl: ''
            },
            form: ''
        },
        _filters: {},
        initialize: function() {
            var me = this;

            me._ajaxOptions = $.extend({}, {
                dataType: 'json',
                context: me
            }, me.options.ajax);

            me._ajax = $.ajax(me._ajaxOptions).done(function(data) {
                var params = $.deserialize();

                me._default = $.extend(true, {}, data);
                me._filters = $.extend(true, {}, data);
                me.$form = $(me.options.form);
                me.$tpl = $(me.options.tpl);
                if (!$.isEmptyObject(params)) {
                    me
                       .setFilters(params, true, true)
                       .trigger('init');
                }
            });
        },
        setFilters: function(params, extended, _start){
            // Метод принимает параметры в виде объекта { key: val } и выставляет атрибут checked в элементах this._filters[key].items
            var me = this,
                prepareStatus = function(filter, item, diapazone){
                    if (filter.type == 'checkbox' && diapazone && filter.status.length && checked) {
                        var regexp = new RegExp('^' + (parseInt(item.name) + 1)),
                            first_status = filter.status[0];
                        if (first_status.search(regexp)){
                            filter.status.unshift(item.name);
                        } else {
                            filter.status.shift();
                            filter.status.unshift(item.name + ' - ' + first_status.substring(first_status.length-4));
                        }
                    } else if (filter.type == 'radio') {
                        checked 
                            && item.id != '' 
                            && filter.status.push(item.name);
                    } else {
                        checked && filter.status.push(item.name);
                    }
                };

            for (var key in params) {
                var filter = me._filters[key];
                if (filter) {
                    filter.count = 0;
                    filter.status = [];
                    filter.extended = extended || filter.extended;
                    filter.update == undefined && (filter.update = true);
                    for (var i = 0; i <= filter.items.length - 1; i++) {
                        var item = filter.items[i],
                            checked = filter.type == 'checkbox' 
                                && (params[key].indexOf(item.id.replace(/\ /g, '+')) >= 0 
                                    || params[key].indexOf(item.id.replace(/\ /g, '%20')) >= 0 
                                    || params[key].indexOf(item.id.replace(/\+/g, '%2B')) >= 0) 
                                || item.id == params[key];

                        prepareStatus(filter, item, filter.name === 'years');
                        checked && filter.count++;
                        item.checked = checked;
                    };
                }
            };
            extended && me._render();
            _start || me.trigger('render');
            return me;
        },
        getFilters: function(){
            return this._filters;
        },
        getParams: function(){
            var params = {};
            for (var key in this._filters) {
                var filter = this._filters[key]; 
                for (var i = filter.items.length - 1; i >= 0; i--) {
                    var item = filter.items[i];
                    if (item.checked && item.id) {
                        var val = item.id;
                        params[key] = params[key] != undefined ? 
                            params[key] + ' ' + val : 
                            val;
                    }
                };
            };
            return params;
        },
        getStatus: function(key){
            var filters = this._filters,
                _status = [];
            
            $.each(this._filters, function(key, item){
                filters[key].status && (_status = _status.concat(filters[key].status));
            });
            return _status.join(', ');
        },
        _render: function(){
            // Метод обновляет фильтры, отображающиеся на странице
            if (typeof this.options.update === 'function'){
                return this.options.update.call(this);
            }
            var me = this,
                filters = me._filters
                tpl = me.$tpl.template();

            for (var key in filters) {
                var $filter = $('.js-filter-' + key),
                    filter = filters[key],
                    condition = filter.extended ? 'checked' : 'default',
                    html = '';
                if (filter.update) {
                    delete filter.update
                    for (var i = 0; i < filter.items.length; i++) {
                        if (filter.items[i][condition] || filter.type == 'radio') {
                            var data = $.extend(filter.items[i], {
                                    key: key,
                                    type: filter.type
                                });
                            html += tpl(data);
                        }
                    };
                    $filter.html(html);
                    $filter
                        .find('.js-formstyler')
                        .styler();
                };
            };
            return me;
        },
        cancel: function(){
            this.trigger('cancel');
            this.trigger('render');
            this._render.call(this);
        },
        reset: function(key) {
            if (key) {
                this._filters[key] = $.extend(true, {}, this._default[key]);
                this._filters[key].update = true;
            } else {
                this._filters = $.extend(true, {}, this._default);
            }
            this.trigger('reset');
            this.trigger('render');
            this._render();
            return this;
        }
    });
})(jQuery);



// jQuery плагин для обработки не загрузившихся картинок
;(function($) {
    
    $.fn.fixImg404 = function(callback, onload, delay) {
        var $me = this,
            uncomplete_onerror = $.browser.msie,
            mark = $.fn.fixImg404.mark,
            handle_error = function(img) {
                callback.call(img, $(img), $me);
            },
            check = function(img, delay) {
                setTimeout(function() {
                    if(img.naturalWidth === undefined) {
                        if(img.readyState === 'uninitialized') {
                            return handle_error(img);
                        }
                    }

                    if(img.complete && img.naturalWidth === 0) {
                        return handle_error(img);
                    }

                    if(img.complete && img.naturalWidth < 10) {
                        return handle_error(img);
                    }
                    
                    if(!img.complete && uncomplete_onerror && img.naturalWidth === 0) {
                        return handle_error(img);
                    }
                    
                    onload && onload.call(img, $(img), $me);
                }, delay || 0);
            };

        var fix = function(delay) {
            $me.find('img:not(.' + mark + ')').addClass(mark).each(function() {
                check(this, delay);
            }).on('error.fix404', function() {
                check(this);
            });
        };
        
        delay = delay || 100;
        
        fix();
        return function() {
            return fix(delay);
        };
    };

    $.fn.fixImg404.mark = '_fixImg404-' + Math.round(Math.random()*1000);

})(jQuery);
